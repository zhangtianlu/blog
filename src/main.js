import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 栅格系统
import { Row, Column } from 'vue-grid-responsive'
Vue.component('row', Row)
Vue.component('column', Column)

// 图片懒加载
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload)

// md编辑器
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
Vue.use(mavonEditor)

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount('#app')
